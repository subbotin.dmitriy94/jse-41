package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.AbstractException_Exception;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-registry";
    }

    @Override
    @NotNull
    public String description() {
        return "User registry.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        endpointLocator
                .getAdminUserEndpoint()
                .createUser(session, login, password, email);
        TerminalUtil.printMessage("[User registered]");
    }

}
