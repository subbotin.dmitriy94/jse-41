package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().findByIdTask(session, id);
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().updateByIdTask(session, id, name, description);
        TerminalUtil.printMessage("[Task updated]");
    }

}
