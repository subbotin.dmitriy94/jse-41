package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.IAdminUserEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.dto.Session;
import com.tsconsulting.dsubbotin.tm.dto.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public User createUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, Role.USER, email);
    }

    @Override
    @NotNull
    @WebMethod
    public List<User> findAllUser(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @NotNull
    @WebMethod
    public User findByIdUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @NotNull
    @WebMethod
    public User findByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public User findByIndexUser(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByIndex(index);
    }

    @Override
    @WebMethod
    public void removeByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void removeByIdUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @Override
    @WebMethod
    public void removeByIndexUser(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByIndex(index);
    }

    @Override
    @WebMethod
    public void clearUser(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @Override
    @WebMethod
    public void setRoleUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "role") Role role
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().setRole(id, role);
    }

    @Override
    @WebMethod
    public void lockByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockByLogin(login);
    }

}
