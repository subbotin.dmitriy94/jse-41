package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.dto.Project;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final SqlSession session) {
        return session.getMapper(IProjectRepository.class);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Project project) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String projectId = project.getId();
            if (projectRepository.findByUserIdAndProjectId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByUserIdAndProjectId(userId, projectId);
            session.commit();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            if (projectRepository.findByUserIdAndProjectId(userId, id) == null) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, id);
            projectRepository.removeByUserIdAndProjectId(userId, id);
            session.commit();
        }
    }

    @Override
    public void removeByIndex(@NotNull String userId, int index) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final String projectId = project.getId();
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByUserIdAndProjectId(userId, projectId);
            session.commit();
        }
    }

    @Override
    public void clear() {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.clear();
            session.commit();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final List<Project> projects = projectRepository.findAll();
            if (projects == null) throw new ProjectNotFoundException();
            return projects;
        }
    }

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.existById(userId, id);
            return project != null;
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(
            @NotNull final String userId,
            @Nullable final String sort
    ) throws AbstractException {
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            @NotNull final Comparator<Project> comparator = sortType.getComparator();
            return findAll(userId, comparator);
        } catch (UnknownSortException exception) {
            try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
                @NotNull final IProjectRepository projectRepository = getRepository(session);
                @Nullable final List<Project> projects = projectRepository.findAllByUserId(userId);
                if (projects == null) throw new ProjectNotFoundException();
                return projects;
            }

        }
    }

    @Override
    @NotNull
    public List<Project> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<Project> comparator
    ) throws AbstractException {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final List<Project> projects = projectRepository.findAllByUserId(userId);
            if (projects == null) throw new ProjectNotFoundException();
            projects.sort(comparator);
            return projects;
        }
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.add(project);
            session.commit();
        }
        return project;
    }

    @Override
    public void addAll(@NotNull List<Project> projects) {
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            for (@NotNull final Project project : projects) projectRepository.add(project);
            session.commit();
        }
    }

    @Override
    @NotNull
    public Project findById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    @NotNull
    public Project findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    @NotNull
    public Project findByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @Nullable final Project project = projectRepository.findByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.updateById(userId, id, name, description);
            session.commit();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkIndex(index);
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            projectRepository.updateByIndex(userId, id, name, description);
            session.commit();
        }
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.startById(userId, id, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            projectRepository.startByIndex(userId, id, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.startByName(userId, name, Status.IN_PROGRESS.toString());
            session.commit();
        }
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishById(userId, id, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            projectRepository.finishByIndex(userId, id, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.finishByName(userId, name, Status.COMPLETED.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.updateStatusById(userId, id, status.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            @NotNull final String id = findByIndex(userId, index).getId();
            projectRepository.updateStatusByIndex(userId, id, status.toString());
            session.commit();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final SqlSession session = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = getRepository(session);
            projectRepository.updateStatusByName(userId, name, status.toString());
            session.commit();
        }
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

}
