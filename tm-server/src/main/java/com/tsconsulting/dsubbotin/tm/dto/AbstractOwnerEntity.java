package com.tsconsulting.dsubbotin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Id
    @NotNull
    @Column(name = "user_id")
    private String userId = UUID.randomUUID().toString();

    @NotNull
    @Override
    public String toString() {
        return super.toString() + " - User Id: " + userId + "; ";
    }

}