package com.tsconsulting.dsubbotin.tm.dto;

import com.tsconsulting.dsubbotin.tm.api.entity.IWBS;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.util.DateAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
@XmlRootElement(name = "Project")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Project extends AbstractOwnerEntity implements IWBS {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @XmlElement
    @Column(name = "start_date")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date startDate = null;

    @NotNull
    @XmlElement
    @Column(name = "create_date")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createDate = new Date();

    @NotNull
    @Override
    public String toString() {
        return super.toString() + name + "; " +
                "Status: " + status + "; " +
                "Started: " + startDate + "; " +
                "Created: " + createDate + ";";
    }

}