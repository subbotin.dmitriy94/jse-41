package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.IAdminEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.dto.Session;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint() {
        super(null);
    }

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataLoadJsonJaxb(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataLoadJsonJaxb();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataSaveJsonJaxb(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataSaveJsonJaxb();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataLoadXmlJaxb(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getDomainService().dataLoadXmlJaxb();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataSaveXmlJaxb(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataSaveXmlJaxb();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataLoadJsonFasterXml(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataLoadJsonFasterXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataSaveJsonFasterXml(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataSaveJsonFasterXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataLoadXmlFasterXml(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataLoadXmlFasterXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataSaveXmlFasterXml(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().dataSaveXmlFasterXml();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadBackup(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getDomainService().loadBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveBackup(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBackup();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadBinary(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBinary();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveBinary(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBinary();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void loadBase64(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBase64();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void saveBase64(
            @Nullable @WebParam(name = "session") Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBase64();
    }

}
