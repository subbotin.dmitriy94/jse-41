package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.dto.Project;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void updateByIndex(
            @NotNull String userId,
            int index,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void addAll(@NotNull List<Project> projects);

    @NotNull
    List<Project> findAll() throws AbstractException;

    void clear();

    void startById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void startByIndex(@NotNull String userId, int index) throws AbstractException;

    void startByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void finishById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void finishByIndex(@NotNull String userId, int index) throws AbstractException;

    void finishByName(@NotNull String userId, @NotNull String name
    ) throws AbstractException;

    void updateStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByIndex(
            @NotNull String userId,
            int index,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByName(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    ) throws AbstractException;

}
