package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.Session;
import com.tsconsulting.dsubbotin.tm.dto.Task;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectTaskEndpoint {

    @WebMethod
    void bindTaskToProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException;

    @WebMethod
    void unbindTaskFromProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId") String taskId
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<Task> findAllTasksByProjectId(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException;

    @WebMethod
    void removeAllProject(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException;

}
