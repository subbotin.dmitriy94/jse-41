package com.tsconsulting.dsubbotin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sessions")
public final class Session extends AbstractEntity implements Cloneable {

    @Column
    @NotNull
    private Date date;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column
    private String signature;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }


}
