package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.dto.Project;
import com.tsconsulting.dsubbotin.tm.dto.User;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProject";

    @NotNull
    private final String userId;

    @NotNull
    private final SqlSession session;

    public ProjectRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService sessionService = new ConnectionService(propertyService);
        session = sessionService.getSqlSession();
        projectRepository = session.getMapper(IProjectRepository.class);
        userRepository = session.getMapper(IUserRepository.class);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        userRepository.add(user);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        session.commit();
    }

    @Before
    public void initializeTest() {
        projectRepository.add(project);
        session.commit();
    }

    @Test
    public void findProject() {
        checkProject(projectRepository.findByName(userId, projectName));
        checkProject(projectRepository.findById(projectId));
        checkProject(projectRepository.findByUserIdAndProjectId(userId, projectId));
        checkProject(projectRepository.findByIndex(userId, 0));
    }

    private void checkProject(@Nullable final Project foundProject) {
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project.getId(), foundProject.getId());
        Assert.assertEquals(project.getName(), foundProject.getName());
        Assert.assertEquals(project.getDescription(), foundProject.getDescription());
        Assert.assertEquals(project.getUserId(), foundProject.getUserId());
        Assert.assertEquals(project.getStartDate(), foundProject.getStartDate());
    }

    @Test
    public void removeByName() {
        Assert.assertNotNull(project);
        projectRepository.removeByName(userId, projectName);
        session.commit();
        Assert.assertNull(projectRepository.findByName(userId, projectName));
    }

    @Test
    public void updateById() {
        @NotNull final String name = "projectNameUpd";
        @NotNull final String description = "projectNDescriptionUpd";
        projectRepository.updateById(userId, projectId, name, description);
        session.commit();
        @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertNotEquals(projectName, project.getName());
        Assert.assertNotEquals(projectDescription, project.getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String name = "projectNameUpd";
        @NotNull final String description = "projectNDescriptionUpd";
        projectRepository.updateByIndex(userId, projectId, name, description);
        session.commit();
        @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
        Assert.assertNotEquals(projectName, project.getName());
        Assert.assertNotEquals(projectDescription, project.getDescription());
    }

    @Test
    public void startById() {
        projectRepository.startById(userId, projectId, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() {
        projectRepository.startByIndex(userId, projectId, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByIndex(userId, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() {
        projectRepository.startByName(userId, projectName, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() {
        projectRepository.finishById(userId, projectId, Status.COMPLETED.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() {
        projectRepository.finishByIndex(userId, projectId, Status.COMPLETED.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByIndex(userId, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() {
        projectRepository.finishByName(userId, projectName, Status.COMPLETED.toString());
        session.commit();
        @Nullable final Project project = projectRepository.findByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() {
        projectRepository.updateStatusById(userId, projectId, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable Project project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusById(userId, projectId, Status.COMPLETED.toString());
        session.commit();
        project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
        projectRepository.updateStatusById(userId, projectId, Status.NOT_STARTED.toString());
        session.commit();
        project = projectRepository.findByUserIdAndProjectId(userId, projectId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() {
        projectRepository.updateStatusByIndex(userId, projectId, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable Project project = projectRepository.findByIndex(userId, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusByIndex(userId, projectId, Status.COMPLETED.toString());
        session.commit();
        project = projectRepository.findByIndex(userId, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
        projectRepository.updateStatusByIndex(userId, projectId, Status.NOT_STARTED.toString());
        session.commit();
        project = projectRepository.findByIndex(userId, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() {
        projectRepository.updateStatusByName(userId, projectName, Status.IN_PROGRESS.toString());
        session.commit();
        @Nullable Project project = projectRepository.findByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusByName(userId, projectName, Status.COMPLETED.toString());
        session.commit();
        project = projectRepository.findByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.COMPLETED);
        projectRepository.updateStatusByName(userId, projectName, Status.NOT_STARTED.toString());
        session.commit();
        project = projectRepository.findByName(userId, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
    }

    @After
    public void finalizeTest() {
        projectRepository.clearById(userId);
        userRepository.removeById(userId);
        session.commit();
        session.close();
    }

}